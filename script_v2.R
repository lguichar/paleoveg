### script_v2.R : Compilation de fonctions permettant d'obtenir la table
### de transtition finale ADN pour un taxon donné
### Stage IRSTEA 2019 Guichardon Lucie
#-----------------------------------------------------------------------

library(raster)

# Repertoires
#======================================================================
dat <- setwd("Z:/Private/Gitlab/paleoveg_2/paleoveg_2")
data_pollen <- "Z:/Private/Gitlab/paleoveg_2/paleoveg_2/data_pollen/"
data_adn <- "Z:/Private/Gitlab/paleoveg_2/paleoveg_2/data_pollen/lacs/"
hyde.folder <- "Z:/Private/Gitlab/paleoveg_2/paleoveg_2/data/dataM2_Paleo/hyde3.2/baseline/asc/"

# Fichiers
#======================================================================
grid.asc <- "data/garea_cr.asc"
temp.grid <- "data/TAV_ky01BP-ky21BP_WGS84.grd"
prec.grid <- "data/PRC_ky01BP-ky21BP_WGS84.grd"
dem1km <- "data/EarthEnv_DEM1km.tif"
soil.rst <- "data/STU_EU_T_SAND.rst"

# Parametres
#======================================================================
cereales = c('Cerealia-type','Cerealia-type (excl. Secale)','Cerealia-type/Secale',
             'Cerealia-type/Triticum','Cerealia-type/Triticum/Avena','Cerealia-type/Zea' ,
             'Secale cereale')

soil.CRS = "+init=epsg:3035"

site = "lathuile"
taxa = c("Fagus","Fagus.1","Fagus.2","Fagaceae")
esp = c("Fagus")

################################### script_v2 / 4 sites alpins ##################
  
  # 1. Creation de l'emprise d'etude
  #======================================================================
  source("sa_extent.R")
  sa_extent = create_sa_extent()
  
  # 2. Creation des raster de l'aire d'etude
  #======================================================================
  
  # 4 sites alpins
  source("studyarea_raster.R")
  lacs_raster = lakes_area_raster(data_pollen_lacs,grid.asc,sa_extent)[["site_raster"]]
  lacs = lakes_area_raster(data_pollen_lacs,grid.asc,sa_extent)[["sites"]]
  
  # 3. Preparation des donnees de pollen
  #======================================================================
  library(data.table)
  source("dataprep_pollen.R")
  tab.esp = dataprep_lacs(data_pollen,esp,site)
  count.esp = countprep_lacs(tab.esp)[["count"]]
  pollen.pix = countprep_lacs(tab.esp)[["pollen.pix"]]
  
  source("studyarea_raster.R")
  site_raster = study_area_raster(dat,grid.asc,sa_extent)[["site_raster"]]
  sites = study_area_raster(dat,grid.asc,sa_extent)[["sites"]]
  agri.sp = dataprep('data_pollen/',cereales)
  count.agri = countprep(agri.sp)[["count"]]
  
  counts = read.csv(paste(data_pollen,"counts.csv" ,sep=""), sep = ";") 
  count.tot = countprep(counts)[["count"]]
  
  # Proportion de pollen de cereales
  dat.cer = merge(count.agri,count.tot,by=c("cells","period"))
  names(dat.cer)[3:4] = c("cer.sum","tot.sum")
  dat.cer$prop = (dat.cer$cer.sum)/(dat.cer$tot.sum)
  
  # 4. Table de transition pollen
  #======================================================================
  require(dplyr)
  source("pollen_transition_dataset.R")
  cells = as.numeric(as.character(unique(pollen.pix$cells)))
  count.sum$cells = cells
  tr.tab = build.pollen.transition.dataset(count.sum)
  
  # 5. Preparation des autres variables (climat & altitude)
  #======================================================================
  source("dataprep_vars.R")
  temp = prep.temp.lacs(temp.grid)[["temp.dat"]]
  prec = prep.prec.lacs(prec.grid)[["prec.dat"]]
  elev = prep.elev(dem1km)
  hyde = prep.hyde.lacs(hyde.folder)
  sand = prep.soil(soil.rst, soil.CRS)
  
  # 6. Preparation des donnees ADN
  #======================================================================
  source("dataprep_ADN.R")
  tab.adn = prepadn(data_adn, taxa, site)
  source("adn_transition_dataset.R")
  tr.tab.adn <- adn.tr.dataset(tab.adn)
  adn.final = complete.tr.tab.adn(tr.tab.adn)
  #adn1 = merge(abond, tr.tab.adn, by = c("cells", "t1","t2"), all.x=TRUE)

  # 7. Construction de la table de transition complete
  #======================================================================
  source("complete_transition_dataset.R")
  tr.tab.final = build.complete.transition.dataset(tr.tab, adn.final, temp, prec, dat.cer, hyde, elev, sand)
  tr.tab.final = tr.tab.final[,-6]

  write.table(adn1,file= paste0("outputs/txt/",esp[1],".",site,".adn.txt"), sep ="\t", row.names=F, quote = F)
