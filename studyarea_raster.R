### studyarea_raster.R : 2 fonctions : 1 Permet de selectionner les lacs de 
### l'EPD selon differents criteres (alitude, superficie)
### 2 Idem pour les sites fournis par EDYTEM
### Stage IRSTEA 2019 Guichardon Lucie
#-----------------------------------------------------------------------

library(raster)

################################### Study area raster / Europe #################

study_area_raster <- function(dat, grid.asc, sa_extent)
  
{
  # Sites de l'EPD
  #===================================================================
  csv = read.csv(paste(data_pollen,"sites.csv", sep =""), sep = ";",header=TRUE)
  write.table(csv, file="outputs/txt/sites.txt", sep ="\t", row.names=F, quote = F)
  
  sites = read.table("outputs/txt/sites.txt", sep ="\t", h=TRUE,encoding = "UTF-8",
  comment.char="", quote = "")
  sitesLoc = unique(sites[,c("site_","elevation", "areaofsite", "latdd","londd")])
  #length(unique(sitesLoc$site_)) #1559
  
  sites.coords = SpatialPointsDataFrame(sitesLoc[,c("londd", "latdd")],  
                                        proj4string=CRS("+proj=longlat +datum=WGS84 
                                        +no_defs +ellps=WGS84 +towgs84=0,0,0"), data = sitesLoc)
  #plot(sites.coords)
  #summary(sites.coords)
  
  # Grille issue de HYDE - résolution 50km2
  #==============================================================
  
  ras = raster(grid.asc)
  projection(ras)=projection(sa_extent)
  ras.crop = crop(ras, sa_extent)
  ras.res = resample(ras.crop,sa_extent)
  
  #plot(ras.res)
  #summary(ras.crop)
  
  pix.pollen = raster::extract(ras.res, sites.coords, cellnumbers=TRUE)
  pix.pollen.uni = unique(pix.pollen)
  #nrow(pix.pollen.uni)
  
  sa = ras.res
  sa[!is.na(ras.res)]=0
  sa[pix.pollen.uni[,"cells"]]=1
  
  #plot(sa)
  #points(sites.coords)
  
  #summary(pix.pollen[,"garea_cr"])
  
  pix.pollen = data.frame(pix.pollen)
  sites.coords$cells = pix.pollen$cells
  sites.coords$garea_cr = pix.pollen$garea_cr
  #head(sites.coords@data)
  #length(unique(sites.coords$site_))
  
  #check NAS
  sites.pix = sites.coords[-which(is.na(sites.coords$cells)),]
  #plot(sites.pix)
  #summary(sites.pix)
  
  #saveRDS(sites.coords, "outputs/rds/sites.rds")
  #write.table(sites.pix@data,file="outputs/txt/sites_pixels.txt", sep ="\t", row.names=F, quote = F)
  
  # Selection par la superficie
  #==============================================================
  # head(sites.pix)
  sites.cut = sites.pix[-which(sites.pix$areaofsite<5 | sites.pix$areaofsite>100),]
  
  # plot(sites.pix)
  # points(sites.cut, col = "red")
  #saveRDS(sites.cut, "outputs/rds/sites.rds")
  #write.table(sites.cut@data,file="outputs/txt/sites_pixels.txt", sep ="\t", row.names=F, quote = F)
  
  
  # Selection par une difference d'altitude trop importante
  #==============================================================
  # library(raster)
  elev = raster(dem1km)
  
  # head(sites.cut)
  
  library(sp)
  coords.proj = spTransform(sites.cut, CRS(projection(elev)))
  
  elev.map = extract(elev, coords.proj)
  sites.cut2 = sites.cut[-which((abs(elev.map - sites.cut$elevation))>100),]
  
  #length(unique(sites.cut2$cells))
  
  #plot(sites.pix)
  #points(sites.cut, col = "blue", pch = 2)
  #points(sites.cut2, col = "red")
  #plot(sites.cut2)
  
  #saveRDS(sites.cut2, "outputs/rds/sites.rds")
  write.table(sites.cut2@data,file="outputs/txt/sites_pixels.txt", sep ="\t", row.names=F, quote = F)
  
  #saveRDS(sa, "outputs/rds/study_area_raster.rds")
  
  
  return(list(sites = sites, site_raster = sites.cut2))
}

######################## Study area raster / 4 sites alpins ####################

lakes_area_raster <- function(dat, grid.asc, sa_extent)
  
{
  # sites d'EDYTEM
  #===================================================================
  csv = read.csv(paste(data_pollen,"lacs/sites.csv", sep =""), sep = ";",header=TRUE)
  write.table(csv, file="outputs/txt/sites_lacs.txt", sep ="\t", row.names=F, quote = F)
  
  lacs = read.table("outputs/txt/sites_lacs.txt", sep ="\t", h=TRUE,
  encoding = "UTF-8", comment.char="", quote = "")
  sitesLoc = unique(lacs[,c("site_","elevation", "areaofsite", "latdd","londd")])
  #length(unique(sitesLoc$site_)) #1559
  
  sites.coords = SpatialPointsDataFrame(sitesLoc[,c("londd", "latdd")],  
                                        proj4string=CRS("+proj=longlat +datum=WGS84 
                                        +no_defs +ellps=WGS84 +towgs84=0,0,0"), data = sitesLoc)
  #plot(sites.coords)
  #summary(sites.coords)
  
  # Grille de HYDE - résolution 50km2
  #==============================================================
  
  ras = raster(grid.asc)
  projection(ras)=projection(sa_extent)
  ras.crop = crop(ras, sa_extent)
  ras.res = resample(ras.crop,sa_extent)
  
  #plot(ras.res)
  #summary(ras.crop)
  
  pix.pollen = raster::extract(ras.res, sites.coords, cellnumbers=TRUE)
  pix.pollen.uni = unique(pix.pollen)
  #nrow(pix.pollen.uni)
  
  sa = ras.res
  sa[!is.na(ras.res)]=0
  sa[pix.pollen.uni[,"cells"]]=1
  
  
  #par(mfrow=c(1,1))
  #plot(sa)
  #points(sites.coords)
  
  #summary(pix.pollen[,"garea_cr"])
  
  pix.pollen = data.frame(pix.pollen)
  sites.coords$cells = pix.pollen$cells
  sites.coords$garea_cr = pix.pollen$garea_cr
  #head(sites.coords@data)
  #length(unique(sites.coords$site_))
  
  #check NAS
  #sites.pix = sites.coords[-which(is.na(sites.coords$cells)),]
  sites.coords$areaofsite = as.numeric(as.character(sites.coords$areaofsite))
  #plot(sites.pix)
  #summary(sites.pix)
  
  #saveRDS(sites.coords, "outputs/rds/sites.rds")
  #write.table(sites.pix@data,file="outputs/txt/sites_pixels.txt", sep ="\t", row.names=F, quote = F)
  
  # Selection par la superficie
  #==============================================================
  # head(sites.pix)
  sites.cut = sites.coords[-which(sites.coords$areaofsite<5 | sites.coords$areaofsite>100),]
  
  # plot(sites.pix)
  # points(sites.cut, col = "red")
  #saveRDS(sites.cut, "outputs/rds/sites.rds")
  #write.table(sites.cut@data,file="outputs/txt/sites_pixels.txt", sep ="\t", row.names=F, quote = F)
  
  
  return(list(sites = lacs, site_raster = sites.cut))
}
