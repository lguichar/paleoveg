### script_v1.R : Compilation de fonctions permettant d'obtenir la table
### de transtition finale pollen pour un taxon donné
### Stage IRSTEA 2019 Guichardon Lucie
#-----------------------------------------------------------------------

library(raster)

# Repertoires
#======================================================================
dat <- setwd("Z:/Private/Gitlab/paleoveg_2/paleoveg_2")
data_pollen <- "Z:/Private/Gitlab/paleoveg_2/paleoveg_2/data_pollen/"
data_adn <- "Z:/Private/Gitlab/paleoveg_2/paleoveg_2/data_pollen/lacs/"
hyde.folder <- "Z:/Private/Gitlab/paleoveg_2/paleoveg_2/data/dataM2_Paleo/hyde3.2/baseline/asc/"

# Fichiers
#======================================================================
grid.asc <- "data/garea_cr.asc"
temp.grid <- "data/TAV_ky01BP-ky21BP_WGS84.grd"
prec.grid <- "data/PRC_ky01BP-ky21BP_WGS84.grd"
dem1km <- "data/EarthEnv_DEM1km.tif"
soil.rst <- "data/STU_EU_T_SAND.rst"

# Parametres
#======================================================================
cereales = c('Cerealia-type','Cerealia-type (excl. Secale)','Cerealia-type/Secale',
             'Cerealia-type/Triticum','Cerealia-type/Triticum/Avena','Cerealia-type/Zea' ,
             'Secale cereale')

soil.CRS = "+init=epsg:3035"

# Taxon issus de l'EPD
#======================================================================
  Abies = c("Abies","Abies alba","Abies maroccana","Abies nordmanniana", "Abies sibirica",
        "Abies undifferentiated","Abies: stomata")
  
  Asteraceae = c("Artemisia","Artemisia (Type A)","Artemisia (Type B)","Artemisia (Type C)",
  "Artemisia (Type D)","Artemisia (Type F)","Artemisia alba-type","Artemisia campestris",
  "Artemisia campestris-type","Artemisia cf. A. norvegica","Artemisia cf. A. norvegica (type 1)",
  "Artemisia cf. A. norvegica (type 2)", "Artemisia genipii/A. mutellina","Artemisia herba-alba",
  "Artemisia herba-alba-type","Artemisia norvegica","Artemisia norvegica-type","Artemisia rupestris",
  "Artemisia sl.","Artemisia ss","Artemisia undifferentiated","Artemisia vulgaris",
  "Artemisia vulgaris-type","Artemisia-type","Artemisia: clumps")
  
  Rumex = c("Rumex acetosa-type","Rumex acetosa-type/Oxyria")
  
  Plantago = c("Plantago lanceolata","Plantago lanceolata-type","Plantago media",
  "Plantago media-type","Plantago media/P. montana","Plantago media/P. montana-type")
  
  Tilia = c("Tilia","Tilia cordata","Tilia cordata-type","Tilia cordata/platyphyllos",
  "Tilia indeterminata","Tilia platyphyllos","Tilia platyphyllos subsp. cordifolia",
  "Tilia platyphyllos-type","Tilia sp.","Tilia tomentosa","Tilia undifferentiated",
  "Tilia x vulgaris","Tilia: stomata","Tilia: trichome")
        
  Ulmus = c("Ulmus","Ulmus (type A)","Ulmus (type B)","Ulmus campestris","Ulmus glabra",
  "Ulmus glabra-type","Ulmus laevis","Ulmus minor","Ulmus pumila","Ulmus scabra",
  "Ulmus sp.","Ulmus undifferentiated", "Ulmus-type","Ulmus/Zelkova")
        
  Fagus = c("Fagus","Fagus orientalis","Fagus sylvatica","Fagus: leaves")

  Juniperus = c("Juniperus","Juniperus communis","Juniperus communis-type",
  "Juniperus communis: stomata","Juniperus oxycedrus-type","Juniperus sabina","Juniperus-type",
  "Juniperus/Cupressaceae","Juniperus/Tetraclinis-type","Juniperus: stomata")
esp = Tilia

################################### script_v1 / Europe ##################

  
  # 1. Creation de l'emprise d'etude
  #======================================================================
  source("sa_extent.R")
  sa_extent = create_sa_extent()

  # 2. Creation des raster de l'aire d'etude
  #======================================================================
  
# Europe
  source("studyarea_raster.R")
  site_raster = study_area_raster(dat,grid.asc,sa_extent)[["site_raster"]]
  sites = study_area_raster(dat,grid.asc,sa_extent)[["sites"]]

  # 3. Preparation des donnees de pollen
  #======================================================================
  source("dataprep_pollen.R")
  tab.esp = dataprep(data_pollen,esp)[["tab.sum"]]
  tab.sp = dataprep(data_pollen,esp)[["tab.sp"]]
  count.esp = countprep(tab.esp, tab.sp)[["count"]]
  pollen.pix = countprep(tab.esp, tab.sp)[["pollen.pix"]]

  tab.agri = dataprep(data_pollen,cereales)[["tab.sum"]]
  tab.cer = dataprep(data_pollen,cereales)[["tab.sp"]]
  count.agri = countprep(tab.agri, tab.cer)[["count"]]

  counts = read.csv(paste(data_pollen,"counts.csv" ,sep=""), sep = ";")
  counts$ID = paste(counts$e_, counts$sample_, sep="_")
  count.sp = data.frame(counts[,c("e_", "sample_")])
  counts = counts[, (4:5)]
  colnames(count.sp) = c("e_", "sample_")
  count.tot = countprep(counts, count.sp)[["count"]]

# Proportion de pollen de cereales
  dat.cer = merge(count.agri,count.tot,by=c("cells","period"))
  names(dat.cer)[3:4] = c("cer.sum","tot.sum")
  dat.cer$prop = (dat.cer$cer.sum)/(dat.cer$tot.sum)*100
  #dat.cer$prop = dat.cer$cer.sum

  # 4. Table de transition pollen
  #======================================================================
  require(dplyr)
  source("pollen_transition_dataset.R")
  bornes = countprep(tab.esp, tab.sp)[["bornes"]]
  tr.tab = build.pollen.transition.dataset(count.esp)

  # 5. Preparation des autres variables (climat & altitude)
  #======================================================================
  source("dataprep_vars.R")
  temp = prep.temp(temp.grid)[["temp.dat"]]
  prec = prep.prec(prec.grid)[["prec.dat"]]
  elev = prep.elev(dem1km)
  hyde = prep.hyde(hyde.folder)
  sand = prep.soil(soil.rst, soil.CRS)
  

  # 6. Construction de la table de transition complete
  #======================================================================
  source("complete_transition_dataset.R")
  tr.tab.final = build.complete.transition.dataset(tr.tab, temp, prec, dat.cer, hyde, elev, sand)
  write.table(tr.tab.final,file= paste0("outputs/txt/",esp[1],".tab.txt"), sep ="\t", row.names=F, quote = F)

  
# Graphique de repartition des altitudes et types de sols
  par(mar=c(5,5,4,2))
hist(site.specif$sand, main = "Répartition des types de sol des sites", xlab="Sable (%)",
         ylab="Effectif", family="A", font.main =1,cex.axis = 2,cex.main=2, cex.lab=2)
hist(site.specif$elev, main = "Répartition des altitudes des sites", xlab="Altitude (m)",
     ylab="Effectif", family="A", font.main =1,cex.axis = 2,cex.main=2, cex.lab=2,
     breaks=seq(min(site.specif$elev),max(site.specif$elev),500))
  